﻿using RPNCalculator.Lib;
using System;
using System.Collections.Generic;

class Rpn
{
    public static void Main()
    {
        // Create an RPNParser object to process the input. This object can be optionally
        // instantiated with a Dictionary<char, Operator> containing a key value array.
        // If instantiating with a custom object then all
        RPNParser rpnParser = new RPNParser();
        string line = "";
        // Loop while line is not null (Null is termination)
        while((line = Console.ReadLine()) != null)
        {
            // Validate that the line isn't just an enter. We can simply ignore it.
            if (line != null && line.Length == 0)
                continue;
            
            // Clean up the line just to verify there isn't any meaningless white space.
            line = line.Trim(new char[] { '\t', '\0', '\n', '\r', ' ' });

            // Verify that the letter q wasn't entered. If so quit.
            if (line == "q")
                return;

            try
            {
                Console.WriteLine(rpnParser.Process(line));
            }
            catch (Exception ex)
            {
                // Write out the message thrown out if any invalid input is entered.
                Console.WriteLine(ex.Message);
            }

        }
    }
}