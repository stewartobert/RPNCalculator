﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPNCalculator.Lib
{

    public class RPNParser
    {

        /// <summary>
        /// Math operations
        /// </summary>
        public enum Operation
        {
            Add = 1,
            Subtract = 2,
            Multiply = 3,
            Divide = 4
        }

        /// <summary>
        /// Store the values in a stack so they can be pushed/popped off using a Stack 
        /// which is LIFO (Last in first out)
        /// </summary>
        Stack<double> _values;


        /// <summary>
        /// List of operations formatted in a dictionary for quick lookup and validation on the operator.
        /// </summary>
        private Dictionary<char, Operation> _operations;


        /// <summary>
        /// Constructor. Setup the operations dictionary. Optionally pass a Dictionary<char, Operation>
        /// parameter to override the default char/operation mapping.
        /// </summary>
        /// <param name="operations"></param>
        public RPNParser(Dictionary<char, Operation> operations = null)
        {
            // Initialize the values object
            _values = new Stack<double>();
            if (operations != null)
            {
                _operations = operations;
            }
            else
            {
                // Sets up the _operations dictionary.
                _operations = new Dictionary<char, Operation>(){
                    { '+', Operation.Add },
                    { '-', Operation.Subtract },
                    { '*', Operation.Multiply },
                    { '/', Operation.Divide }
                };
            }
        }

        /// <summary>
        /// Retreives top 2 values from the stack. If there are not two
        /// values within the stack then throws an exception.
        /// </summary>
        /// <returns></returns>
        private double[] GrabTopTwoValues()
        {
            if (_values.Count < 2)
            {
                throw new ArgumentOutOfRangeException("Error, the reverse polish notation requires values to be input prior to performing an operation so a minimum of two values are required.");
            }
            double[] resultValues = new double[2];
            resultValues[1] = _values.Pop();
            resultValues[0] = _values.Pop();
            return resultValues;
        }

        /// <summary>
        /// Add two values together
        /// </summary>
        /// <returns></returns>
        private double Add()
        {
            double[] values = GrabTopTwoValues();
            double result = values[0] + values[1];
            _values.Push(result);
            return result;
        }

        /// <summary>
        /// Subtract two values
        /// </summary>
        /// <returns></returns>
        private double Subtract()
        {
            double[] values = GrabTopTwoValues();
            double result = values[0] - values[1];
            _values.Push(result);
            return result;
        }

        /// <summary>
        /// Multiply two values.
        /// </summary>
        /// <returns></returns>
        private double Multiply()
        {
            double[] values = GrabTopTwoValues();
            double result = values[0] * values[1];
            _values.Push(result);
            return result;
        }

        /// <summary>
        /// Divide Two Values
        /// </summary>
        /// <returns></returns>
        private double Divide()
        {
            double[] values = GrabTopTwoValues();
            double result = values[0] / values[1];
            _values.Push(result);
            return result;
        }

        /// <summary>
        /// Process an operation character
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private double ProcessOperation(char input)
        {
            // Validate that the single character entered is recognized as an operation. 
            // If not throw an exception that contains a list of viable operation characters.
            if (!_operations.ContainsKey(input))
            {

                throw new InvalidOperationException(String.Format("Error, The operation code does not exist. The opeation codes are as follows: {0}.",
                    String.Join(", ", _operations.Keys.ToList())));
            }
            // Character is properly validated as a workable operation char. Set it
            Operation operation = _operations[input];
            switch (operation)
            {
                case Operation.Add:
                    return Add();
                case Operation.Subtract:
                    return Subtract();
                case Operation.Multiply:
                    return Multiply();
                case Operation.Divide:
                    return Divide();
                
            }
            // It should not be possible to reach this point under any circumstances because it has been validated
            // that an acceptable character was input before the operation is ever handled but left in as a final
            // safety measure.
            throw new ArgumentException("Error, inputs must be either valid numbers or operational characters.");
        }
    


        /// <summary>
        /// Process the input. 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public double Process(string input)
        {
            double inputValue = 0;
            if (Double.TryParse(input, out inputValue))
            {
                _values.Push(inputValue);
                return inputValue;
            } else
            {
                if (input.Length != 1)
                {
                    throw new ArgumentException("Error, inputs must be either valid numbers or operational characters.");
                } else
                {
                    return ProcessOperation(input[0]);
                }
            }
            throw new ArgumentException("Error, inputs must be either valid numbers or operational characters.");
        }

    }
}
