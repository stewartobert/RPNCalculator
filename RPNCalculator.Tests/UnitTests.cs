﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPNCalculator.Lib;

namespace RPNCalculator.Tests
{
    [TestClass]
    public class UnitTests
    {

        #region Run several tests using some known inputs and expected output values     

        [TestMethod]
        public void TestSetOne()
        {
            RPNParser calc = new RPNParser();
            Assert.IsTrue(calc.Process("5") == 5);
            Assert.IsTrue(calc.Process("8") == 8);
            Assert.IsTrue(calc.Process("+") == 13);            
        }

        [TestMethod]
        public void TestSetTwo()
        {
            RPNParser calc = new RPNParser();
            Assert.IsTrue(calc.Process("-3") == -3);
            Assert.IsTrue(calc.Process("-2") == -2);
            Assert.IsTrue(calc.Process("*") == 6);
            Assert.IsTrue(calc.Process("5") == 5);
            Assert.IsTrue(calc.Process("+") == 11);            
        }

        [TestMethod]
        public void TestSetThree()
        {
            RPNParser calc = new RPNParser();
            Assert.IsTrue(calc.Process("2") == 2);
            Assert.IsTrue(calc.Process("9") == 9);
            Assert.IsTrue(calc.Process("3") == 3);
            Assert.IsTrue(calc.Process("+") == 12);
            Assert.IsTrue(calc.Process("*") == 24);
            
        }

        [TestMethod]
        public void TestSetFour()
        {
            RPNParser calc = new RPNParser();
            Assert.IsTrue(calc.Process("20") == 20);
            Assert.IsTrue(calc.Process("13") == 13);
            Assert.IsTrue(calc.Process("-") == 7);
            Assert.IsTrue(calc.Process("2") == 2);
            Assert.IsTrue(calc.Process("/") == 3.5);
        }

        [TestMethod]
        public void TestSetFive()
        {
            RPNParser calc = new RPNParser();
            Assert.IsTrue(calc.Process("5") == 5);
            Assert.IsTrue(calc.Process("1") == 1);
            Assert.IsTrue(calc.Process("2") == 2);
            Assert.IsTrue(calc.Process("+") == 3);
            Assert.IsTrue(calc.Process("4") == 4);
            Assert.IsTrue(calc.Process("*") == 12);
            Assert.IsTrue(calc.Process("+") == 17);
            Assert.IsTrue(calc.Process("3") == 3);
            Assert.IsTrue(calc.Process("-") == 14);
        }

        #endregion

        /// <summary>
        /// Validates that inputs that would be expected to cause an exception do
        /// cause an exception.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ValidateRequireTwoElements()
        {
            RPNParser calc = new RPNParser();
            calc.Process("2");
            calc.Process("+");
        }


        /// <summary>
        /// Validates that an invalid input exception occurs when 
        /// a non recognizable character entered.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ValidateInvalidInputException()
        {
            RPNParser calc = new RPNParser();
            calc.Process("abc");
        }


        /// <summary>
        /// Validates that an invalid input exception occurs when 
        /// a non recognizable character is entered.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ValidateOperatorException()
        {
            RPNParser calc = new RPNParser();
            calc.Process("f");
        }
    }
}
